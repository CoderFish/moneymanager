package models

import (
	"reflect"
)

type (
	TUser struct {
		UserModel TUserModel
		State string
	}
	TUserModel struct {
		ID string
		EMail string
		PasswordHash string
		ConfirmHash string
		ExpConfirmHash uint64
		IsUserActive bool
	}
)

//по хорошему нужно переписать так чтобы было приведение типов если тип в бд отличается от типа в модели и дать возможность иметь в модели поля которые заполняются не из бд
func DeSerializeModel(model interface{},arr []interface{}){
	if arr !=nil {
		v := reflect.ValueOf(model)
		cnt := v.Elem().NumField()
		for i := 0; i < cnt; i++ {
			if v.Elem().Field(i).Type().String() == "uint64"{v.Elem().Field(i).SetUint(arr[i].(uint64))}else
			if v.Elem().Field(i).Type().String() == "string"{v.Elem().Field(i).SetString(arr[i].(string))}else
			if v.Elem().Field(i).Type().String() == "bool"{v.Elem().Field(i).SetBool(arr[i].(bool))}
		}
	}
}

//
func SerializeModel(model interface{}) []interface{}{
	v := reflect.ValueOf(model)
	cnt := v.Elem().NumField()
	var result = make([]interface{},cnt)
	for i := 0; i < cnt; i++ {result[i] = v.Elem().Field(i).Interface()}
	return result
}