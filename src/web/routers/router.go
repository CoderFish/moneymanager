package routers

import (
	"web/controllers"
	"github.com/astaxie/beego"
)

func init() {
	//Home
    beego.Router("/", &controllers.MainController{})
    beego.Router("/:lang([a-z]{2})", &controllers.MainController{})

	//Login Register
	beego.Router("/:lang([a-z]{2}/userreg", &controllers.UserRegController{})
	beego.Router("/:lang([a-z]{2}/mail_confirm/*", &controllers.MailConfirmController{})
	beego.Router("/:lang([a-z]{2}/mail_confirm_sended", &controllers.MailConfirmSendedController{})

	beego.Router("/:lang([a-z]{2}/login", &controllers.LoginController{})
	beego.Router("/:lang([a-z]{2}/logout", &controllers.LogOutController{})

	//Profile DashBoard
	beego.Router("/:lang([a-z]{2}/profile", &controllers.ProfileController{})
	beego.Router("/:lang([a-z]{2}/dashboard", &controllers.DashBoardController{})
}
