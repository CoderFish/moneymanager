package conf

import (
	"github.com/astaxie/beego/session"
	"encoding/json"
)

var GlobalSessions *session.Manager

func init() {
	cf := session.ManagerConfig{}
	//вынести в конфиг как нормальный json
	json.Unmarshal([]byte(`{"cookieName":"session_id", "enableSetCookie,omitempty": true, "gclifetime":3600, "maxLifetime": 3600, "secure": false, "sessionIDHashFunc": "sha1", "sessionIDHashKey": "", "cookieLifeTime": 3600, "providerConfig": "./sessions"}`),&cf)
	GlobalSessions, _ = session.NewManager("file", &cf)
	go GlobalSessions.GC()
}
