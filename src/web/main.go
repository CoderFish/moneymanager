package main

import (
	_ "web/routers"
	_ "web/conf"
	"github.com/astaxie/beego"
)

func main() {
	beego.SetStaticPath("/css", "static/css")
	beego.Run()
}

