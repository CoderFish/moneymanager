package controllers

import (
	"web/conf"
	"github.com/astaxie/beego/session"
	"github.com/k0kubun/pp"
	"gopkg.in/gomail.v2"
	"github.com/astaxie/beego"
	"html/template"
	"strings"
	"bytes"
	"web/models"
	"github.com/google/uuid"
	"github.com/jameskeane/bcrypt"
	"time"
)


func (this *BaseController) InitSession()(session session.Store) {
	if ssid := this.Ctx.GetCookie("ssid"); ssid == ""{
		s,err := conf.GlobalSessions.SessionStart(this.Ctx.ResponseWriter, this.Ctx.Request)
		if err!=nil {pp.Println(err)}
		this.Ctx.SetCookie("ssid", s.SessionID())
		return s
	}else{
		s,err := conf.GlobalSessions.GetSessionStore(ssid)
		if err!=nil {pp.Println(err)}
		return s
	}
}

func (this *BaseController)SendConfirmMail(toMail,confirmHash string){
	//переделать с протокол должен по другому сюда ставиться
	templateData := struct {ConfirmCode string}{ConfirmCode: "http://"+this.Ctx.Request.Host+"/"+this.Data["Lng"].(string)+"/mail_confirm/"+confirmHash}

	t, err := template.ParseFiles("views/email_confirm.html")
	if err != nil {pp.Println(err);return}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, templateData); err != nil {pp.Println(err);return}

	m := gomail.NewMessage()
	m.SetHeader("From", beego.AppConfig.String("ma_mail"))
	m.SetHeader("To", toMail)
	m.SetHeader("Subject", "Registration confirmation CF")
	m.SetBody("text/html", buf.String())
	d := gomail.NewDialer(beego.AppConfig.String("ma_host"),
		beego.AppConfig.DefaultInt("ma_port",465),
		beego.AppConfig.String("ma_user"),
		beego.AppConfig.String("ma_pass"))
	if err := d.DialAndSend(m); err != nil {pp.Println(err)}
}

func (this *BaseController)IsAuthorized() bool{
	//пока заглушка поправить чуть позже
	a := this.Sess.Get("is_autorized")
	return a != nil && a == true
}

func (this *BaseController)GetUserByEmail(email string) *models.TUser{
	return this.GetUserByIndex("users_email",strings.ToLower(email))
}

func (this *BaseController)GetUserByIndex(index,val string) *models.TUser{
	tuple, err := this.DB.SelectOne("users",index,val)
	if err != nil{pp.Println(err)}
	var u = models.TUser{}
	models.DeSerializeModel(&u.UserModel,tuple)
	if u.UserModel.IsUserActive {u.State="user_active"}else
	if u.UserModel.ID == "" {u.State="user_not_exist"}else{u.State="user_not_active"}
	return &u
}

func (this *BaseController)InitNewUser(user *models.TUser,IsNew bool){
	if IsNew {
		user.UserModel.ID = strings.Replace(uuid.New().String(),"-","",-1)
		user.UserModel.EMail = strings.ToLower(this.Ctx.Request.Form["email"][0])
	}
	user.UserModel.PasswordHash = GetPasswordHash(this.Ctx.Request.Form["password"][0])
	user.UserModel.ConfirmHash = strings.Replace(uuid.New().String(),"-","",-1)
	user.UserModel.ExpConfirmHash = uint64(time.Now().Unix())+uint64(beego.AppConfig.DefaultInt("expire_mail_hash_after",3600))
	//user.UserModel.IsUserActive = false //неявно уже false
}

func IsEmail(eMail string) bool {
	atCharIndex := strings.Index(eMail,"@")
	if atCharIndex == -1 {return false}
	dotCharIndex := strings.Index(eMail[atCharIndex:],".")
	return atCharIndex != -1 && dotCharIndex != -1
}

func CheckPass(password,confirm string) bool{
	return len(strings.TrimSpace(password))<6 || strings.TrimSpace(password) != strings.TrimSpace(confirm)
}

//пароли подобрать что-то по надёжнее
func GetPasswordHash(password string) string{
	hash, err := bcrypt.Hash(password)
	if err != nil {pp.Println(err)}
	return hash
}

func MatchPassword(password,hash string)bool{
	return bcrypt.Match(password,hash)
}