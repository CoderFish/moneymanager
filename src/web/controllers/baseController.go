package controllers
import (
	"github.com/astaxie/beego"
	"github.com/beego/i18n"
	"strings"
	"github.com/astaxie/beego/session"
	"net/http"
)
var (
	locales = make(map[string]string)
)

type (
	BaseController struct {
		beego.Controller
		i18n.Locale
		Sess session.Store
		DB IDB
	}
)

func init() {
	//Languages
	beego.AddFuncMap("i18n", i18n.Tr)
	langTypes := strings.Split(beego.AppConfig.String("lang_types"), "|")
	for _, lang := range langTypes {
		beego.Trace("Loading language: " + lang)
		locales[lang[0:2]] = lang
		if err := i18n.SetMessage(lang, "conf/"+"locale_"+lang+".ini"); err != nil {
			beego.Error("Fail to set message file:", err)
			return
		}
	}
}

func (this *BaseController) Prepare() {
	this.DB = NewDB()
	this.Sess = this.InitSession()
	this.setLanguage()
	//так делать не стоит это просто для тренировки
	this.PrepareHeader()
	if this.Lang == "" || locales[this.Lang[0:2]] == "" {http.NotFound(this.Ctx.ResponseWriter,this.Ctx.Request)}
}

func (this *BaseController) Finish(){defer this.Sess.SessionRelease(this.Ctx.ResponseWriter)}

func (this *BaseController) setLanguage(){
	if len(this.Ctx.Request.RequestURI)>2{
		this.Lang = locales[this.Ctx.Request.RequestURI[1:3]]
	}else{
		this.Lang = ""
		lang := this.Ctx.Request.Header.Get("Accept-Language")
		if len(lang) > 4 {
			lang = lang[:5]
			if i18n.IsExist(lang) {this.Lang = lang}
		}
	}
	this.Data["Lang"] = this.Lang
}