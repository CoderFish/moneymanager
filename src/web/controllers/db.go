package controllers

import (
	"github.com/astaxie/beego"
	"github.com/k0kubun/pp"
)

type(
	IDB interface {
		SelectOne (tableName,indexFieldName,fieldVal string) (data []interface{}, err error)
		InsertOne (tableName string,tuple interface{})
		UpdateOne (tableName string,tuple interface{})
	}
)

func NewDB () IDB{
	if beego.AppConfig.String("db_name") == "tarantool"{return new(TDBTarantool)}
	pp.Println("В конфиге не указан db_name")
	return nil
}