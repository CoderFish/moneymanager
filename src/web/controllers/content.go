package controllers

import (
	"github.com/astaxie/beego"
	"strings"
	"html/template"
)

func (this *BaseController) PrepareHeader(){
	var loc []template.HTML
	str := ""
	if len(this.Ctx.Request.RequestURI)>4 {str = this.Ctx.Request.RequestURI[4:]}
	for key,_ := range locales{
		if locales[key] != this.Lang {
			loc = append(loc,beego.Str2html(`<a class="dropdown-item" href="/`+key+`/`+str+`">`+strings.ToUpper(key)+`</a>`+"\n"))
		}
	}
	if len(this.Lang)>1{this.Data["Locale"] = strings.ToUpper(this.Lang[0:2])}
	this.Data["LangList"] = loc
	this.Data["Lng"] = this.Lang[0:2]
	this.Data["isAuthorized"] = this.IsAuthorized()
}

