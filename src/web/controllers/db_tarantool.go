package controllers

import (
	"github.com/tarantool/go-tarantool"
	"time"
	"github.com/astaxie/beego"
	"github.com/k0kubun/pp"
)

type(
	TDBTarantool struct {}
)

var (
	DBServer =  beego.AppConfig.String("db_host")+":"+beego.AppConfig.String("db_port")
	DBOpts = tarantool.Opts{
		Timeout:       500 * time.Millisecond,
		Reconnect:     1000 * time.Millisecond,
		MaxReconnects: 3,
		User:		   beego.AppConfig.String("db_user"),
		Pass:		   beego.AppConfig.String("db_pass"),
	}
)

//а вообще на init открываем коннект и на finish закрываем

func (this *TDBTarantool) SelectOne (tableName,indexFieldName,fieldVal string) (data []interface{}, err error){
	client, err := tarantool.Connect(DBServer, DBOpts)
	if err != nil {pp.Errorf("Failed to connect: %s", err.Error());return}
	result, err := client.Select(tableName, indexFieldName, 0, 1, tarantool.IterEq, []interface{}{string(fieldVal)})
	client.Close()
	if len(result.Data)==0 {return}
	return result.Data[0].([]interface{}),err
}

func (this *TDBTarantool) InsertOne (tableName string,tuple interface{}){
	client, err := tarantool.Connect(DBServer, DBOpts)
	if err != nil {pp.Errorf("Failed to connect: %s", err.Error());return}
	result, err := client.Insert(tableName,tuple)
	client.Close()
	if err != nil {pp.Errorf("Error: %s", err.Error());return}
	if result.Error != "" {pp.Errorf("Insert Error: %s", result.Error);return}
}
//а вообще для моих нужд есть Replace это Update or Insert
func (this *TDBTarantool) UpdateOne (tableName string,tuple interface{}){
	client, err := tarantool.Connect(DBServer, DBOpts)
	if err != nil {pp.Errorf("Failed to connect: %s", err.Error());return}
	result, err := client.Replace(tableName,tuple)
	client.Close()
	if err != nil {pp.Errorf("Error: %s", err.Error());return}
	if result.Error != "" {pp.Errorf("Insert Error: %s", result.Error);return}
}