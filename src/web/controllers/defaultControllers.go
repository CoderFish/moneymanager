package controllers

import (
	"github.com/k0kubun/pp"
	"web/models"
	"strings"
)

type (
	MainController struct {BaseController}
	LoginController struct {BaseController}
	UserRegController struct {BaseController}
	ProfileController struct {BaseController}
	DashBoardController struct {BaseController}
	MailConfirmController struct {BaseController}
	LogOutController struct {BaseController}
	MailConfirmSendedController struct {BaseController}
)

func (this *MainController) Get() {
	this.TplName = "index.html"
}

func (this *UserRegController) Get() {
	this.TplName = "user_reg.html"
}

func (this *UserRegController) Post() {
	//проверка чтоб email был email'ом
	if !IsEmail(this.Ctx.Request.Form["email"][0]) {
		pp.Println("Это не почта, реализовать обратную связь")
	}
	//проверка паролей
	if !CheckPass(this.Ctx.Request.Form["password"][0],this.Ctx.Request.Form["confirm_password"][0]){
		pp.Println("Проблема c паролем, реализовать обратную связь")
	}
	user := this.GetUserByEmail(this.Ctx.Request.Form["email"][0])
	if 	user.State == "user_active" {
		pp.Println("Такой пользователь уже есть, реализовать обратную связь")
		this.TplName = "user_reg.html"
	}else
	if user.State == "user_not_exist"{
		this.InitNewUser(user,true)
		this.DB.InsertOne("users",models.SerializeModel(&user.UserModel))
		go this.SendConfirmMail(user.UserModel.EMail,user.UserModel.ConfirmHash)
		this.Redirect("/"+this.Data["Lng"].(string)+"/mail_confirm_sended",302)
	}else
	if user.State == "user_not_active"{
		this.InitNewUser(user,false)
		this.DB.UpdateOne("users",models.SerializeModel(&user.UserModel))
		go this.SendConfirmMail(user.UserModel.EMail,user.UserModel.ConfirmHash)
		this.Redirect("/"+this.Data["Lng"].(string)+"/mail_confirm_sended",302)
	}
}

func (this *MailConfirmSendedController) Get() {
	this.TplName = "email_confirmation_sended.html"
}

func (this *MailConfirmController) Get() {
	user := this.GetUserByIndex("users_email_confirm_hash",strings.Split(this.Ctx.Request.RequestURI,"/")[3])
	if user.UserModel.IsUserActive{
		this.TplName = "email_already_confirmed.html"
	}else{
		if user.UserModel.ID != "" {
			user.UserModel.IsUserActive = true
			this.DB.UpdateOne("users",models.SerializeModel(&user.UserModel))
			this.TplName = "email_confirmed.html"
		}else{
			this.TplName = "email_confirm_error.html"
		}
	}

}



//когда будешь подтверждать email не забудь проверить expiration_time