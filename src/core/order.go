package core

import (
	"github.com/pharrisee/poloniex-api"
	"github.com/k0kubun/pp"
	"strconv"
)

type (
	TOrder struct{
		mm *TMoneyManager
		OrderType string
		Pair string
		OrderKind string
		Rate float64
		Amount float64
		OrderNumber int64
	}
)

func NewOrder(orderType,orderKind string,pair string, rate, amount float64, mm *TMoneyManager) *TOrder {
	o := new(TOrder)
	o.mm = mm
	o.OrderType = orderType
	o.OrderKind = orderKind
	o.Pair = pair
	o.Rate = rate
	o.Amount = amount
	return o
}

func (o *TOrder) PutOrder() (err error){
pp.Println("Пробую поставить "+o.OrderKind+": "+o.Pair+" "+o.OrderType+" amount: "+strconv.FormatFloat(o.Amount,'g',-1,64)+" rate: "+strconv.FormatFloat(o.Rate,'g',-1,64))
	if o.OrderType == "buy"{
		var bresult poloniex.Buy
		bresult,err = o.mm.exchange.Buy(o.Pair,o.Rate,o.Amount)
//проверить если ошибка то что находиться в bresult
		o.OrderNumber = bresult.OrderNumber
	}
	if o.OrderType == "sell"{
		var sresult poloniex.Sell
		sresult,err = o.mm.exchange.Sell(o.Pair,o.Rate,o.Amount)
//проверить если ошибка то что находиться в sresult
		o.OrderNumber = sresult.OrderNumber
	}
//сообщение в bd
	o.mm.Serialize()
	return err
}

func (o *TOrder) CanceOrder() (success bool, err error){
	if o.OrderNumber != 0 {
pp.Println("Пробую снять "+o.OrderKind+": "+strconv.Itoa(int(o.OrderNumber)))
		success,err = o.mm.exchange.CancelOrder(o.OrderNumber)
		if success {o.OrderNumber = 0}
	}
//сообщение в bd
	o.mm.Serialize()
	return success, err
}

func (o *TOrder) SellPostOnly() (err error){
pp.Println("Пробую поставить "+o.OrderKind+" PostOnly: "+o.Pair+" "+o.OrderType+" amount: "+strconv.FormatFloat(o.Amount,'g',-1,64)+" rate: "+strconv.FormatFloat(o.Rate,'g',-1,64))
	var sresult poloniex.Sell
	sresult,err = o.mm.exchange.SellPostOnly(o.Pair,o.Rate,o.Amount)
	//проверить если ошибка то что находиться в sresult
	o.OrderNumber = sresult.OrderNumber
//сообщение в БД
	o.mm.Serialize()
	return err
}