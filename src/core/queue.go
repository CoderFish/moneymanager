package core

import (
	"time"
)

/****************************************
 *	Очередь выполнения задач
 *	Приоритезация (high,normal)
 *	Сдвигает менее приоритетные задачи
*****************************************/

var Sleep time.Duration = 2
type (
	IJob interface {
		do()
		getPriority() *string
		getExecTime() *int64
	}
	TQueue struct {
		jobs []*IJob
	}
	TJob struct{
		priority string
		execTime int64
		closure func()
	}
)
////////////////////Export functions
func NewJob(priority string, timeShift int64, closure func()) *TJob{
	result := new(TJob)
	result.priority=priority
	result.execTime = time.Now().Unix()+timeShift
	result.closure=closure
	return result
}
func NewQueue() *TQueue{
	result := new(TQueue)
	go result.run()
	return result
}
func (q *TQueue) AddJobs(job ...IJob) {
	for i:=0; i<len(job); i++ {
		q.jobs = append(q.jobs,&job[i])
	}
}
////////////////////Private functions
func (q *TQueue) do(){
	if len(q.jobs)>0 {
		var hp int64 = 0
		var np int64 = 0
		var hpi = 0
		var npi = 0
		for i, j := range q.jobs {
			if (*IJob(*j).getPriority() == "high") && ((hp == 0) || (*IJob(*j).getExecTime() < hp)) {
				hp = *IJob(*j).getExecTime()
				hpi = i
			}
			if (*IJob(*j).getPriority() == "normal") && ((np == 0) || (*IJob(*j).getExecTime() < np)) {
				np = *IJob(*j).getExecTime()
				npi = i
			}
		}
		if (hp != 0)&&(*IJob(*q.jobs[hpi]).getExecTime() <= time.Now().Unix()) {
			go IJob(*q.jobs[hpi]).do()
			q.deleteJob(hpi)
		}else
		if (np != 0)&&(*IJob(*q.jobs[npi]).getExecTime() <= time.Now().Unix()) {
			go IJob(*q.jobs[npi]).do()
			q.deleteJob(npi)
		}
	}
}
func (q *TQueue) run() {
	for {
		q.do()
		time.Sleep(Sleep*time.Second)
	}
}
func (q *TQueue) deleteJob(index int){
	copy(q.jobs[index:], q.jobs[index+1:])
	q.jobs[len(q.jobs)-1] = nil
	q.jobs = q.jobs[:len(q.jobs)-1]
}
func (j *TJob) do() {
	j.closure()
}
func (j *TJob) getPriority() *string {
	return &j.priority
}
func (j *TJob) getExecTime() *int64 {
	return &j.execTime
}

