package core

import (
	"github.com/pharrisee/poloniex-api"
	"github.com/chuckpreslar/emission"
	"github.com/pkg/errors"
	"math/rand"
	"time"
	"github.com/k0kubun/pp"
	"strconv"
)

var tickers = poloniex.Ticker{
	"BTC_FLDC": poloniex.TickerEntry{
		Last:        0.000003,
		Ask:         0.000003,
		Bid:         0.000003,
		Change:      0.015748,
		BaseVolume:  3.068772,
		QuoteVolume: 1205256.432818,
		IsFrozen:    0,
		ID:          31,
	},
	"BTC_OMNI": poloniex.TickerEntry{
			Last:        0.003813,
			Ask:         0.003814,
			Bid:         0.003778,
			Change:      -0.020010,
			BaseVolume:  4.045055,
			QuoteVolume: 1065.355513,
			IsFrozen:    0,
			ID:          58,
		},
	"BTC_NEOS": poloniex.TickerEntry{
			Last:        0.000415,
			Ask:         0.000415,
			Bid:         0.000414,
			Change:      0.088618,
			BaseVolume:  5.508113,
			QuoteVolume: 13465.854366,
			IsFrozen:    0,
			ID:          63,
		},
	"BTC_XPM": poloniex.TickerEntry{
			Last:        0.000069,
			Ask:         0.000070,
			Bid:         0.000069,
			Change:      0.088739,
			BaseVolume:  5.242368,
			QuoteVolume: 76736.908440,
			IsFrozen:    0,
			ID:          116,
		},
	"BTC_EXP": poloniex.TickerEntry{
			Last:        0.000257,
			Ask:         0.000258,
			Bid:         0.000258,
			Change:      0.003481,
			BaseVolume:  4.363201,
			QuoteVolume: 16802.854229,
			IsFrozen:    0,
			ID:          153,
		},
	"BTC_STRAT": poloniex.TickerEntry{
			Last:        0.000732,
			Ask:         0.000732,
			Bid:         0.000730,
			Change:      -0.033485,
			BaseVolume:  31.225072,
			QuoteVolume: 41974.736897,
			IsFrozen:    0,
			ID:          182,
		},
	"BTC_GNT": poloniex.TickerEntry{
			Last:        0.000039,
			Ask:         0.000039,
			Bid:         0.000039,
			Change:      0.165763,
			BaseVolume:  53.270774,
			QuoteVolume: 1490149.627048,
			IsFrozen:    0,
			ID:          185,
		},
	"BTC_DOGE": poloniex.TickerEntry{
			Last:        0.000001,
			Ask:         0.000001,
			Bid:         0.000001,
			Change:      -0.015625,
			BaseVolume:  60.517438,
			QuoteVolume: 95709066.358410,
			IsFrozen:    0,
			ID:          27,
		},
	"ETH_CVC": poloniex.TickerEntry{
			Last:        0.000409,
			Ask:         0.000421,
			Bid:         0.000410,
			Change:      -0.010889,
			BaseVolume:  3.473544,
			QuoteVolume: 8396.264998,
			IsFrozen:    0,
			ID:          195,
		},
	"BTC_ZEC": poloniex.TickerEntry{
			Last:        0.040540,
			Ask:         0.040659,
			Bid:         0.040541,
			Change:      -0.004656,
			BaseVolume:  117.287948,
			QuoteVolume: 2878.190113,
			IsFrozen:    0,
			ID:          178,
		},
	"USDT_XMR": poloniex.TickerEntry{
			Last:        275.294097,
			Ask:         275.294097,
			Bid:         272.270695,
			Change:      0.015845,
			BaseVolume:  648317.879617,
			QuoteVolume: 2387.557975,
			IsFrozen:    0,
			ID:          126,
		},
	"USDT_STR": poloniex.TickerEntry{
			Last:        0.342746,
			Ask:         0.345577,
			Bid:         0.345577,
			Change:      -0.006419,
			BaseVolume:  1072205.948401,
			QuoteVolume: 3152824.056254,
			IsFrozen:    0,
			ID:          125,
		},
	"BTC_SC": poloniex.TickerEntry{
			Last:        0.000002,
			Ask:         0.000002,
			Bid:         0.000002,
			Change:      0.038278,
			BaseVolume:  85.340999,
			QuoteVolume: 40104422.166206,
			IsFrozen:    0,
			ID:          150,
		},
	"BTC_DCR": poloniex.TickerEntry{
			Last:        0.007400,
			Ask:         0.007400,
			Bid:         0.007384,
			Change:      -0.026391,
			BaseVolume:  29.338104,
			QuoteVolume: 3906.909533,
			IsFrozen:    0,
			ID:          162,
		},
	"ETH_STEEM": poloniex.TickerEntry{
			Last:        0.003723,
			Ask:         0.003723,
			Bid:         0.003723,
			Change:      -0.040387,
			BaseVolume:  11.059639,
			QuoteVolume: 2954.190260,
			IsFrozen:    0,
			ID:          169,
		},
	"USDT_BCH": poloniex.TickerEntry{
			Last:        1162.657501,
			Ask:         1170.682197,
			Bid:         1166.815618,
			Change:      -0.014358,
			BaseVolume:  2418604.602690,
			QuoteVolume: 2082.954292,
			IsFrozen:    0,
			ID:          191,
		},
	"BTC_HUC": poloniex.TickerEntry{
			Last:        0.000025,
			Ask:         0.000025,
			Bid:         0.000025,
			Change:      0.087765,
			BaseVolume:  6.689661,
			QuoteVolume: 266966.874787,
			IsFrozen:    0,
			ID:          43,
		},
	"BTC_DASH": poloniex.TickerEntry{
			Last:        0.060888,
			Ask:         0.061044,
			Bid:         0.060888,
			Change:      -0.005097,
			BaseVolume:  101.820143,
			QuoteVolume: 1656.436984,
			IsFrozen:    0,
			ID:          24,
		},
	"BTC_PINK": poloniex.TickerEntry{
			Last:        0.000003,
			Ask:         0.000003,
			Bid:         0.000003,
			Change:      0.000000,
			BaseVolume:  2.031365,
			QuoteVolume: 727991.810741,
			IsFrozen:    0,
			ID:          73,
		},
	"BTC_PPC": poloniex.TickerEntry{
			Last:        0.000320,
			Ask:         0.000320,
			Bid:         0.000317,
			Change:      -0.003084,
			BaseVolume:  6.510916,
			QuoteVolume: 20665.693847,
			IsFrozen:    0,
			ID:          75,
		},
	"BTC_VTC": poloniex.TickerEntry{
			Last:        0.000391,
			Ask:         0.000390,
			Bid:         0.000389,
			Change:      0.039362,
			BaseVolume:  19.829070,
			QuoteVolume: 52195.025175,
			IsFrozen:    0,
			ID:          100,
		},
	"BTC_LSK": poloniex.TickerEntry{
			Last:        0.002034,
			Ask:         0.002040,
			Bid:         0.002030,
			Change:      -0.022192,
			BaseVolume:  84.035525,
			QuoteVolume: 40865.158433,
			IsFrozen:    0,
			ID:          163,
		},
	"ETH_ZEC": poloniex.TickerEntry{
			Last:        0.459356,
			Ask:         0.459415,
			Bid:         0.457000,
			Change:      -0.018339,
			BaseVolume:  91.666797,
			QuoteVolume: 197.872866,
			IsFrozen:    0,
			ID:          179,
		},
	"ETH_OMG": poloniex.TickerEntry{
			Last:        0.019173,
			Ask:         0.019173,
			Bid:         0.018919,
			Change:      -0.045294,
			BaseVolume:  30.346964,
			QuoteVolume: 1551.631377,
			IsFrozen:    0,
			ID:          197,
		},
	"BTC_CLAM": poloniex.TickerEntry{
			Last:        0.000563,
			Ask:         0.000563,
			Bid:         0.000556,
			Change:      -0.047384,
			BaseVolume:  4.807774,
			QuoteVolume: 8488.798659,
			IsFrozen:    0,
			ID:          20,
		},
	"ETH_GAS": poloniex.TickerEntry{
			Last:        0.044057,
			Ask:         0.044340,
			Bid:         0.044058,
			Change:      -0.012906,
			BaseVolume:  18.507080,
			QuoteVolume: 421.420896,
			IsFrozen:    0,
			ID:          199,
		},
	"XMR_BLK": poloniex.TickerEntry{
			Last:        0.001220,
			Ask:         0.001224,
			Bid:         0.001200,
			Change:      -0.035945,
			BaseVolume:  1.051522,
			QuoteVolume: 862.054754,
			IsFrozen:    0,
			ID:          130,
		},
	"BTC_ETH": poloniex.TickerEntry{
			Last:        0.088748,
			Ask:         0.088748,
			Bid:         0.088670,
			Change:      0.021423,
			BaseVolume:  1502.600509,
			QuoteVolume: 17082.132154,
			IsFrozen:    0,
			ID:          148,
		},
	"BTC_ZRX": poloniex.TickerEntry{
			Last:        0.000100,
			Ask:         0.000101,
			Bid:         0.000100,
			Change:      0.017104,
			BaseVolume:  87.148952,
			QuoteVolume: 856235.582224,
			IsFrozen:    0,
			ID:          192,
		},
	"BTC_EMC2": poloniex.TickerEntry{
			Last:        0.000030,
			Ask:         0.000031,
			Bid:         0.000030,
			Change:      -0.011986,
			BaseVolume:  8.935671,
			QuoteVolume: 286628.079338,
			IsFrozen:    0,
			ID:          28,
		},
	"BTC_XEM": poloniex.TickerEntry{
			Last:        0.000040,
			Ask:         0.000040,
			Bid:         0.000040,
			Change:      -0.019807,
			BaseVolume:  148.626349,
			QuoteVolume: 3712029.529035,
			IsFrozen:    0,
			ID:          112,
		},
	"ETH_ETC": poloniex.TickerEntry{
			Last:        0.040000,
			Ask:         0.040246,
			Bid:         0.040000,
			Change:      -0.052070,
			BaseVolume:  1152.226053,
			QuoteVolume: 27947.766435,
			IsFrozen:    0,
			ID:          172,
		},
	"BTC_STR": poloniex.TickerEntry{
			Last:        0.000036,
			Ask:         0.000036,
			Bid:         0.000036,
			Change:      -0.000563,
			BaseVolume:  299.822978,
			QuoteVolume: 8359128.054255,
			IsFrozen:    0,
			ID:          89,
		},
	"BTC_VIA": poloniex.TickerEntry{
			Last:        0.000265,
			Ask:         0.000268,
			Bid:         0.000265,
			Change:      -0.086516,
			BaseVolume:  14.712405,
			QuoteVolume: 52947.163490,
			IsFrozen:    0,
			ID:          97,
		},
	"BTC_XBC": poloniex.TickerEntry{
			Last:        0.007051,
			Ask:         0.007063,
			Bid:         0.007051,
			Change:      -0.016414,
			BaseVolume:  1.313087,
			QuoteVolume: 187.611563,
			IsFrozen:    0,
			ID:          104,
		},
	"USDT_NXT": poloniex.TickerEntry{
			Last:        0.205609,
			Ask:         0.205609,
			Bid:         0.203903,
			Change:      -0.014356,
			BaseVolume:  773796.859411,
			QuoteVolume: 3772567.712295,
			IsFrozen:    0,
			ID:          124,
		},
	"BTC_DGB": poloniex.TickerEntry{
			Last:        0.000003,
			Ask:         0.000003,
			Bid:         0.000003,
			Change:      0.020588,
			BaseVolume:  83.481472,
			QuoteVolume: 23834959.494146,
			IsFrozen:    0,
			ID:          25,
		},
	"BTC_AMP": poloniex.TickerEntry{
			Last:        0.000030,
			Ask:         0.000030,
			Bid:         0.000030,
			Change:      0.095359,
			BaseVolume:  17.580169,
			QuoteVolume: 580135.446315,
			IsFrozen:    0,
			ID:          160,
		},
	"BTC_ETC": poloniex.TickerEntry{
			Last:        0.003566,
			Ask:         0.003566,
			Bid:         0.003566,
			Change:      -0.032618,
			BaseVolume:  509.936175,
			QuoteVolume: 140938.122944,
			IsFrozen:    0,
			ID:          171,
		},
	"BTC_MAID": poloniex.TickerEntry{
			Last:        0.000036,
			Ask:         0.000037,
			Bid:         0.000036,
			Change:      -0.015667,
			BaseVolume:  67.121632,
			QuoteVolume: 1832712.267882,
			IsFrozen:    0,
			ID:          51,
		},
	"BTC_GRC": poloniex.TickerEntry{
			Last:        0.000006,
			Ask:         0.000006,
			Bid:         0.000006,
			Change:      0.032787,
			BaseVolume:  2.746133,
			QuoteVolume: 435890.033716,
			IsFrozen:    0,
			ID:          40,
		},
	"BTC_LTC": poloniex.TickerEntry{
			Last:        0.023320,
			Ask:         0.023320,
			Bid:         0.023310,
			Change:      0.056682,
			BaseVolume:  1710.986123,
			QuoteVolume: 74940.308858,
			IsFrozen:    0,
			ID:          50,
		},
	"USDT_DASH": poloniex.TickerEntry{
			Last:        593.999980,
			Ask:         593.999980,
			Bid:         583.819866,
			Change:      0.004150,
			BaseVolume:  565491.093150,
			QuoteVolume: 952.904650,
			IsFrozen:    0,
			ID:          122,
		},
	"XMR_BTCD": poloniex.TickerEntry{
			Last:        0.337358,
			Ask:         0.347992,
			Bid:         0.343893,
			Change:      -0.011781,
			BaseVolume:  4.700764,
			QuoteVolume: 13.348735,
			IsFrozen:    0,
			ID:          131,
		},
	"XMR_LTC": poloniex.TickerEntry{
			Last:        0.824410,
			Ask:         0.828261,
			Bid:         0.824410,
			Change:      0.050564,
			BaseVolume:  149.676369,
			QuoteVolume: 186.711837,
			IsFrozen:    0,
			ID:          137,
		},
	"USDT_ETH": poloniex.TickerEntry{
			Last:        858.000015,
			Ask:         859.000000,
			Bid:         858.000015,
			Change:      0.019171,
			BaseVolume:  5224485.953750,
			QuoteVolume: 6242.691900,
			IsFrozen:    0,
			ID:          149,
		},
	"BTC_BCY": poloniex.TickerEntry{
			Last:        0.000048,
			Ask:         0.000048,
			Bid:         0.000048,
			Change:      0.010331,
			BaseVolume:  2.084540,
			QuoteVolume: 43523.041054,
			IsFrozen:    0,
			ID:          151,
		},
	"BTC_BTM": poloniex.TickerEntry{
			Last:        0.000076,
			Ask:         0.000077,
			Bid:         0.000076,
			Change:      0.024587,
			BaseVolume:  5.230548,
			QuoteVolume: 63254.859205,
			IsFrozen:    0,
			ID:          13,
		},
	"USDT_REP": poloniex.TickerEntry{
			Last:        43.272345,
			Ask:         43.272345,
			Bid:         43.194987,
			Change:      -0.000156,
			BaseVolume:  166127.782714,
			QuoteVolume: 3854.555613,
			IsFrozen:    0,
			ID:          175,
		},
	"BTC_ARDR": poloniex.TickerEntry{
			Last:        0.000042,
			Ask:         0.000042,
			Bid:         0.000041,
			Change:      -0.026838,
			BaseVolume:  8.541231,
			QuoteVolume: 198233.396575,
			IsFrozen:    0,
			ID:          177,
		},
	"BTC_NXC": poloniex.TickerEntry{
			Last:        0.000020,
			Ask:         0.000020,
			Bid:         0.000020,
			Change:      0.017000,
			BaseVolume:  2.831332,
			QuoteVolume: 138336.214570,
			IsFrozen:    0,
			ID:          183,
		},
	"BTC_BCH": poloniex.TickerEntry{
			Last:        0.120689,
			Ask:         0.120898,
			Bid:         0.120518,
			Change:      -0.007984,
			BaseVolume:  210.525964,
			QuoteVolume: 1724.186489,
			IsFrozen:    0,
			ID:          189,
		},
	"ETH_BCH": poloniex.TickerEntry{
			Last:        1.368000,
			Ask:         1.367998,
			Bid:         1.356000,
			Change:      -0.029787,
			BaseVolume:  119.864192,
			QuoteVolume: 86.768645,
			IsFrozen:    0,
			ID:          190,
		},
	"BTC_OMG": poloniex.TickerEntry{
			Last:        0.001685,
			Ask:         0.001693,
			Bid:         0.001685,
			Change:      -0.030254,
			BaseVolume:  58.969098,
			QuoteVolume: 34124.128805,
			IsFrozen:    0,
			ID:          196,
		},
	"BTC_SBD": poloniex.TickerEntry{
			Last:        0.000345,
			Ask:         0.000345,
			Bid:         0.000342,
			Change:      -0.002375,
			BaseVolume:  1.370405,
			QuoteVolume: 3978.902637,
			IsFrozen:    0,
			ID:          170,
		},
	"BTC_BURST": poloniex.TickerEntry{
			Last:        0.000003,
			Ask:         0.000003,
			Bid:         0.000003,
			Change:      -0.021277,
			BaseVolume:  10.306496,
			QuoteVolume: 3135132.567862,
			IsFrozen:    0,
			ID:          15,
		},
	"BTC_SYS": poloniex.TickerEntry{
			Last:        0.000060,
			Ask:         0.000061,
			Bid:         0.000060,
			Change:      0.026361,
			BaseVolume:  13.836948,
			QuoteVolume: 230582.516015,
			IsFrozen:    0,
			ID:          92,
		},
	"BTC_VRC": poloniex.TickerEntry{
			Last:        0.000073,
			Ask:         0.000073,
			Bid:         0.000073,
			Change:      0.034872,
			BaseVolume:  5.712609,
			QuoteVolume: 79970.430533,
			IsFrozen:    0,
			ID:          99,
		},
	"XMR_BCN": poloniex.TickerEntry{
			Last:        0.000016,
			Ask:         0.000016,
			Bid:         0.000016,
			Change:      -0.017715,
			BaseVolume:  3.279920,
			QuoteVolume: 203438.831569,
			IsFrozen:    0,
			ID:          129,
		},
	"XMR_MAID": poloniex.TickerEntry{
			Last:        0.001289,
			Ask:         0.001300,
			Bid:         0.001284,
			Change:      -0.009890,
			BaseVolume:  6.705681,
			QuoteVolume: 5281.820788,
			IsFrozen:    0,
			ID:          138,
		},
	"XMR_NXT": poloniex.TickerEntry{
			Last:        0.000749,
			Ask:         0.000754,
			Bid:         0.000750,
			Change:      -0.021601,
			BaseVolume:  1.529930,
			QuoteVolume: 2014.177569,
			IsFrozen:    0,
			ID:          140,
		},
	"BTC_BCN": poloniex.TickerEntry{
			Last:        0.000000,
			Ask:         0.000000,
			Bid:         0.000000,
			Change:      0.000000,
			BaseVolume:  19.435166,
			QuoteVolume: 42158975.668975,
			IsFrozen:    0,
			ID:          7,
		},
	"BTC_GAME": poloniex.TickerEntry{
			Last:        0.000210,
			Ask:         0.000210,
			Bid:         0.000208,
			Change:      -0.041537,
			BaseVolume:  227.501109,
			QuoteVolume: 1090064.278373,
			IsFrozen:    0,
			ID:          38,
		},
	"BTC_NAV": poloniex.TickerEntry{
			Last:        0.000176,
			Ask:         0.000177,
			Bid:         0.000176,
			Change:      -0.007120,
			BaseVolume:  3.312336,
			QuoteVolume: 18668.679956,
			IsFrozen:    0,
			ID:          61,
		},
	"BTC_XVC": poloniex.TickerEntry{
			Last:        0.000043,
			Ask:         0.000043,
			Bid:         0.000042,
			Change:      0.027811,
			BaseVolume:  2.341079,
			QuoteVolume: 53970.509818,
			IsFrozen:    0,
			ID:          98,
		},
	"BTC_XRP": poloniex.TickerEntry{
			Last:        0.000093,
			Ask:         0.000093,
			Bid:         0.000093,
			Change:      -0.011716,
			BaseVolume:  585.279638,
			QuoteVolume: 6244156.299948,
			IsFrozen:    0,
			ID:          117,
		},
	"BTC_BTS": poloniex.TickerEntry{
			Last:        0.000022,
			Ask:         0.000022,
			Bid:         0.000022,
			Change:      -0.010497,
			BaseVolume:  71.801913,
			QuoteVolume: 3288578.559913,
			IsFrozen:    0,
			ID:          14,
		},
	"BTC_FLO": poloniex.TickerEntry{
			Last:        0.000010,
			Ask:         0.000010,
			Bid:         0.000010,
			Change:      -0.043129,
			BaseVolume:  2.061500,
			QuoteVolume: 210885.219806,
			IsFrozen:    0,
			ID:          32,
		},
	"BTC_NXT": poloniex.TickerEntry{
			Last:        0.000021,
			Ask:         0.000021,
			Bid:         0.000021,
			Change:      -0.017153,
			BaseVolume:  41.373648,
			QuoteVolume: 1917512.279685,
			IsFrozen:    0,
			ID:          69,
		},
	"USDT_XRP": poloniex.TickerEntry{
			Last:        0.896566,
			Ask:         0.900785,
			Bid:         0.896566,
			Change:      -0.018000,
			BaseVolume:  3689151.915728,
			QuoteVolume: 4134421.964209,
			IsFrozen:    0,
			ID:          127,
		},
	"BTC_STEEM": poloniex.TickerEntry{
			Last:        0.000332,
			Ask:         0.000331,
			Bid:         0.000331,
			Change:      -0.010972,
			BaseVolume:  9.275314,
			QuoteVolume: 27744.235406,
			IsFrozen:    0,
			ID:          168,
		},
	"BTC_REP": poloniex.TickerEntry{
			Last:        0.004462,
			Ask:         0.004462,
			Bid:         0.004458,
			Change:      0.004080,
			BaseVolume:  44.738065,
			QuoteVolume: 9853.973610,
			IsFrozen:    0,
			ID:          174,
		},
	"BTC_GNO": poloniex.TickerEntry{
			Last:        0.013514,
			Ask:         0.013512,
			Bid:         0.013357,
			Change:      0.020637,
			BaseVolume:  8.402458,
			QuoteVolume: 628.192234,
			IsFrozen:    0,
			ID:          187,
		},
	"BTC_BLK": poloniex.TickerEntry{
			Last:        0.000034,
			Ask:         0.000034,
			Bid:         0.000034,
			Change:      -0.038181,
			BaseVolume:  1.452033,
			QuoteVolume: 42190.622073,
			IsFrozen:    0,
			ID:          10,
		},
	"BTC_NMC": poloniex.TickerEntry{
			Last:        0.000261,
			Ask:         0.000261,
			Bid:         0.000258,
			Change:      -0.025705,
			BaseVolume:  2.528896,
			QuoteVolume: 9745.411207,
			IsFrozen:    0,
			ID:          64,
		},
	"BTC_XCP": poloniex.TickerEntry{
			Last:        0.002229,
			Ask:         0.002262,
			Bid:         0.002229,
			Change:      0.037776,
			BaseVolume:  10.152368,
			QuoteVolume: 4659.953829,
			IsFrozen:    0,
			ID:          108,
		},
	"BTC_XMR": poloniex.TickerEntry{
			Last:        0.028251,
			Ask:         0.028332,
			Bid:         0.028251,
			Change:      0.014406,
			BaseVolume:  445.729743,
			QuoteVolume: 15642.262816,
			IsFrozen:    0,
			ID:          114,
		},
	"XMR_ZEC": poloniex.TickerEntry{
			Last:        1.442000,
			Ask:         1.442291,
			Bid:         1.428442,
			Change:      -0.008088,
			BaseVolume:  88.454477,
			QuoteVolume: 62.043606,
			IsFrozen:    0,
			ID:          181,
		},
	"BTC_CVC": poloniex.TickerEntry{
			Last:        0.000036,
			Ask:         0.000036,
			Bid:         0.000036,
			Change:      -0.001101,
			BaseVolume:  9.229618,
			QuoteVolume: 252079.332022,
			IsFrozen:    0,
			ID:          194,
		},
	"BTC_STORJ": poloniex.TickerEntry{
			Last:        0.000091,
			Ask:         0.000092,
			Bid:         0.000091,
			Change:      0.007520,
			BaseVolume:  9.455970,
			QuoteVolume: 100971.750332,
			IsFrozen:    0,
			ID:          200,
		},
	"BTC_BELA": poloniex.TickerEntry{
			Last:        0.000019,
			Ask:         0.000019,
			Bid:         0.000019,
			Change:      0.133294,
			BaseVolume:  9.483553,
			QuoteVolume: 530322.013604,
			IsFrozen:    0,
			ID:          8,
		},
	"ETH_LSK": poloniex.TickerEntry{
			Last:        0.022942,
			Ask:         0.022942,
			Bid:         0.022942,
			Change:      -0.048547,
			BaseVolume:  61.967863,
			QuoteVolume: 2656.194791,
			IsFrozen:    0,
			ID:          166,
		},
	"BTC_LBC": poloniex.TickerEntry{
			Last:        0.000035,
			Ask:         0.000035,
			Bid:         0.000035,
			Change:      0.061669,
			BaseVolume:  15.052278,
			QuoteVolume: 423063.018600,
			IsFrozen:    0,
			ID:          167,
		},
	"USDT_ETC": poloniex.TickerEntry{
			Last:        34.580000,
			Ask:         34.599999,
			Bid:         34.580000,
			Change:      -0.034366,
			BaseVolume:  4285426.302838,
			QuoteVolume: 124036.341519,
			IsFrozen:    0,
			ID:          173,
		},
	"BTC_PASC": poloniex.TickerEntry{
			Last:        0.000157,
			Ask:         0.000157,
			Bid:         0.000157,
			Change:      -0.087778,
			BaseVolume:  24.161319,
			QuoteVolume: 145841.623220,
			IsFrozen:    0,
			ID:          184,
		},
	"BTC_RIC": poloniex.TickerEntry{
			Last:        0.000035,
			Ask:         0.000035,
			Bid:         0.000034,
			Change:      0.194118,
			BaseVolume:  238.476482,
			QuoteVolume: 7383378.562977,
			IsFrozen:    0,
			ID:          83,
		},
	"BTC_POT": poloniex.TickerEntry{
			Last:        0.000016,
			Ask:         0.000016,
			Bid:         0.000016,
			Change:      0.009311,
			BaseVolume:  4.218995,
			QuoteVolume: 260694.873039,
			IsFrozen:    0,
			ID:          74,
		},
	"BTC_FCT": poloniex.TickerEntry{
			Last:        0.003151,
			Ask:         0.003160,
			Bid:         0.003151,
			Change:      0.083783,
			BaseVolume:  34.237431,
			QuoteVolume: 11201.214127,
			IsFrozen:    0,
			ID:          155,
		},
	"ETH_REP": poloniex.TickerEntry{
			Last:        0.050405,
			Ask:         0.050715,
			Bid:         0.050428,
			Change:      -0.028105,
			BaseVolume:  97.978704,
			QuoteVolume: 1891.480515,
			IsFrozen:    0,
			ID:          176,
		},
	"ETH_GNT": poloniex.TickerEntry{
			Last:        0.000435,
			Ask:         0.000435,
			Bid:         0.000432,
			Change:      0.132990,
			BaseVolume:  91.340903,
			QuoteVolume: 219329.136385,
			IsFrozen:    0,
			ID:          186,
		},
	"ETH_GNO": poloniex.TickerEntry{
			Last:        0.151608,
			Ask:         0.151608,
			Bid:         0.150500,
			Change:      -0.002655,
			BaseVolume:  20.851610,
			QuoteVolume: 137.899459,
			IsFrozen:    0,
			ID:          188,
		},
	"ETH_ZRX": poloniex.TickerEntry{
			Last:        0.001133,
			Ask:         0.001136,
			Bid:         0.001129,
			Change:      -0.002781,
			BaseVolume:  73.654809,
			QuoteVolume: 63143.043021,
			IsFrozen:    0,
			ID:          193,
		},
	"BTC_BTCD": poloniex.TickerEntry{
			Last:        0.009680,
			Ask:         0.009769,
			Bid:         0.009675,
			Change:      0.026227,
			BaseVolume:  2.587446,
			QuoteVolume: 266.221559,
			IsFrozen:    0,
			ID:          12,
		},
	"USDT_LTC": poloniex.TickerEntry{
			Last:        226.000000,
			Ask:         226.000000,
			Bid:         225.333040,
			Change:      0.051652,
			BaseVolume:  6955792.920434,
			QuoteVolume: 32140.490421,
			IsFrozen:    0,
			ID:          123,
		},
	"XMR_DASH": poloniex.TickerEntry{
			Last:        2.148146,
			Ask:         2.148146,
			Bid:         2.148146,
			Change:      -0.017316,
			BaseVolume:  56.964858,
			QuoteVolume: 26.685420,
			IsFrozen:    0,
			ID:          132,
		},
	"BTC_RADS": poloniex.TickerEntry{
			Last:        0.000572,
			Ask:         0.000577,
			Bid:         0.000569,
			Change:      -0.114498,
			BaseVolume:  17.514345,
			QuoteVolume: 28645.476607,
			IsFrozen:    0,
			ID:          158,
		},
	"USDT_ZEC": poloniex.TickerEntry{
			Last:        395.000000,
			Ask:         395.000000,
			Bid:         391.819299,
			Change:      0.002205,
			BaseVolume:  378319.410026,
			QuoteVolume: 976.769031,
			IsFrozen:    0,
			ID:          180,
		},
	"BTC_GAS": poloniex.TickerEntry{
			Last:        0.003926,
			Ask:         0.003930,
			Bid:         0.003926,
			Change:      0.016530,
			BaseVolume:  11.504544,
			QuoteVolume: 2992.776156,
			IsFrozen:    0,
			ID:          198,
		},
	"USDT_BTC": poloniex.TickerEntry{
			Last:        9689.728858,
			Ask:         9689.728858,
			Bid:         9688.465681,
			Change:      -0.004344,
			BaseVolume:  25564691.814792,
			QuoteVolume: 2695.025559,
			IsFrozen:    0,
			ID:          121,
		},
}

var openOrdersAll = poloniex.OpenOrdersAll{
	"BTC_FLO":   poloniex.OpenOrders{},
	"BTC_LBC":   poloniex.OpenOrders{},
	"BTC_SC":    poloniex.OpenOrders{},
	"BTC_XPM":   poloniex.OpenOrders{},
	"ETH_STEEM": poloniex.OpenOrders{},
	"USDT_ETC":  poloniex.OpenOrders{},
	"BTC_BURST": poloniex.OpenOrders{},
	"BTC_CLAM":  poloniex.OpenOrders{},
	"XMR_BTCD":  poloniex.OpenOrders{},
	"BTC_GNT":   poloniex.OpenOrders{},
	"BTC_GRC":   poloniex.OpenOrders{},
	"BTC_LTC":   poloniex.OpenOrders{},
	"BTC_PINK":  poloniex.OpenOrders{},
	"BTC_RIC":   poloniex.OpenOrders{},
	"BTC_SYS":   poloniex.OpenOrders{},
	"BTC_BCH":   poloniex.OpenOrders{},
	"BTC_CVC":   poloniex.OpenOrders{},
	"BTC_VRC":   poloniex.OpenOrders{},
	"BTC_LSK":   poloniex.OpenOrders{},
	"BTC_PPC":   poloniex.OpenOrders{},
	"BTC_RADS":  poloniex.OpenOrders{},
	"BTC_STORJ": poloniex.OpenOrders{},
	"BTC_XRP":   poloniex.OpenOrders{},
	"ETH_ETC":   poloniex.OpenOrders{},
	"BTC_ARDR":  poloniex.OpenOrders{},
	"BTC_BTCD":  poloniex.OpenOrders{},
	"ETH_ZEC":   poloniex.OpenOrders{},
	"XMR_NXT":   poloniex.OpenOrders{},
	"BTC_STRAT": poloniex.OpenOrders{
		poloniex.OpenOrder{
			OrderNumber: 35947299899,
			Type:        "sell",
			Rate:        0.002300,
			Amount:      24.937500,
			Total:       0.057356,
		},
	},
	"ETH_ZRX":   poloniex.OpenOrders{},
	"XMR_MAID":  poloniex.OpenOrders{},
	"BTC_OMG":   poloniex.OpenOrders{},
	"ETH_LSK":   poloniex.OpenOrders{},
	"BTC_XEM":   poloniex.OpenOrders{},
	"ETH_GNO":   poloniex.OpenOrders{},
	"ETH_GNT":   poloniex.OpenOrders{},
	"ETH_OMG":   poloniex.OpenOrders{},
	"XMR_LTC":   poloniex.OpenOrders{},
	"BTC_ETH":   poloniex.OpenOrders{},
	"BTC_PASC":  poloniex.OpenOrders{},
	"BTC_OMNI":  poloniex.OpenOrders{},
	"ETH_BCH":   poloniex.OpenOrders{},
	"BTC_DASH":  poloniex.OpenOrders{},
	"BTC_EMC2":  poloniex.OpenOrders{},
	"XMR_BCN":   poloniex.OpenOrders{},
	"BTC_FLDC":  poloniex.OpenOrders{},
	"BTC_HUC":   poloniex.OpenOrders{},
	"BTC_XCP":   poloniex.OpenOrders{},
	"BTC_ZEC":   poloniex.OpenOrders{},
	"USDT_NXT":  poloniex.OpenOrders{},
	"USDT_REP":  poloniex.OpenOrders{},
	"BTC_BCN":   poloniex.OpenOrders{},
	"BTC_BTM":   poloniex.OpenOrders{},
	"BTC_NAV":   poloniex.OpenOrders{},
	"BTC_SBD":   poloniex.OpenOrders{},
	"BTC_XBC":   poloniex.OpenOrders{},
	"BTC_XMR":   poloniex.OpenOrders{},
	"USDT_XRP":  poloniex.OpenOrders{},
	"BTC_ETC":   poloniex.OpenOrders{},
	"BTC_MAID":  poloniex.OpenOrders{},
	"BTC_POT":   poloniex.OpenOrders{},
	"BTC_REP":   poloniex.OpenOrders{},
	"BTC_VIA":   poloniex.OpenOrders{},
	"ETH_GAS":   poloniex.OpenOrders{},
	"USDT_DASH": poloniex.OpenOrders{},
	"USDT_STR":  poloniex.OpenOrders{},
	"BTC_BCY":   poloniex.OpenOrders{},
	"BTC_DCR":   poloniex.OpenOrders{},
	"USDT_ZEC":  poloniex.OpenOrders{},
	"BTC_STEEM": poloniex.OpenOrders{},
	"ETH_CVC":   poloniex.OpenOrders{},
	"USDT_ETH":  poloniex.OpenOrders{},
	"XMR_BLK":   poloniex.OpenOrders{},
	"XMR_DASH":  poloniex.OpenOrders{},
	"XMR_ZEC":   poloniex.OpenOrders{},
	"BTC_FCT":   poloniex.OpenOrders{},
	"BTC_NXC":   poloniex.OpenOrders{},
	"BTC_GNO":   poloniex.OpenOrders{},
	"BTC_NEOS":  poloniex.OpenOrders{},
	"BTC_NXT":   poloniex.OpenOrders{},
	"BTC_XVC":   poloniex.OpenOrders{},
	"BTC_ZRX":   poloniex.OpenOrders{},
	"USDT_BTC":  poloniex.OpenOrders{},
	"BTC_BTS":  poloniex.OpenOrders{},
	"BTC_DGB":  poloniex.OpenOrders{},
	"USDT_XMR": poloniex.OpenOrders{},
	"BTC_GAS":  poloniex.OpenOrders{},
	"USDT_BCH": poloniex.OpenOrders{},
	"BTC_BLK":  poloniex.OpenOrders{},
	"BTC_EXP":  poloniex.OpenOrders{},
	"BTC_DOGE": poloniex.OpenOrders{},
	"BTC_GAME": poloniex.OpenOrders{},
	"BTC_NMC":  poloniex.OpenOrders{},
	"BTC_STR":  poloniex.OpenOrders{},
	"BTC_VTC":  poloniex.OpenOrders{},
	"ETH_REP":  poloniex.OpenOrders{},
	"BTC_AMP":  poloniex.OpenOrders{},
	"BTC_BELA": poloniex.OpenOrders{},
	"USDT_LTC": poloniex.OpenOrders{},
}

var tradeHistory = poloniex.PrivateTradeHistoryAll{
	"USDT_BTC": poloniex.PrivateTradeHistory{
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-02-05 18:35:55",
		Rate:          7200.000000,
		Amount:        0.088673,
		Total:         638.445816,
		OrderNumber:   144517734724,
		Type:          "sell",
		GlobalTradeID: 340641971,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-02-05 18:02:53",
		Rate:          7080.100000,
		Amount:        0.088806,
		Total:         628.756989,
		OrderNumber:   144504807664,
		Type:          "buy",
		GlobalTradeID: 340618809,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-18 02:21:39",
		Rate:          11600.470000,
		Amount:        0.026025,
		Total:         301.904668,
		OrderNumber:   137630969383,
		Type:          "sell",
		GlobalTradeID: 330386870,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-16 08:32:21",
		Rate:          11510.000000,
		Amount:        0.026064,
		Total:         299.999978,
		OrderNumber:   136813604566,
		Type:          "buy",
		GlobalTradeID: 328095484,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-11 03:48:58",
		Rate:          13500.000000,
		Amount:        0.022222,
		Total:         299.999970,
		OrderNumber:   134999985001,
		Type:          "buy",
		GlobalTradeID: 323619975,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-02 06:03:11",
		Rate:          13099.000000,
		Amount:        0.015010,
		Total:         196.610357,
		OrderNumber:   131739242008,
		Type:          "sell",
		GlobalTradeID: 312373836,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-02 06:03:08",
		Rate:          13099.000000,
		Amount:        0.003110,
		Total:         40.737628,
		OrderNumber:   131739242008,
		Type:          "sell",
		GlobalTradeID: 312373798,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-02 06:03:07",
		Rate:          13099.000000,
		Amount:        0.040987,
		Total:         536.890809,
		OrderNumber:   131739242008,
		Type:          "sell",
		GlobalTradeID: 312373792,
	},
		poloniex.PrivateTradeHistoryEntry{
		Date:          "2018-01-02 06:03:06",
		Rate:          13099.000000,
		Amount:        0.011792,
		Total:         154.459347,
		OrderNumber:   131739242008,
		Type:          "sell",
		GlobalTradeID: 312373778,
	},
	},
	"BTC_GNO": poloniex.PrivateTradeHistory{
		poloniex.PrivateTradeHistoryEntry{
			Date:          "2018-01-16 06:06:54",
			Rate:          0.021005,
			Amount:        1.056362,
			Total:         0.022189,
			OrderNumber:   35464466221,
			Type:          "buy",
			GlobalTradeID: 327952841,
		},
	},
	"BTC_STEEM": poloniex.PrivateTradeHistory{
		poloniex.PrivateTradeHistoryEntry{
			Date:          "2018-01-02 05:58:26",
			Rate:          0.000470,
			Amount:        56.532957,
			Total:         0.026570,
			OrderNumber:   55587238287,
			Type:          "sell",
			GlobalTradeID: 312370317,
		},
		poloniex.PrivateTradeHistoryEntry{
			Date:          "2018-01-02 05:58:23",
			Rate:          0.000470,
			Amount:        0.879721,
			Total:         0.000413,
			OrderNumber:   55587238287,
			Type:          "sell",
			GlobalTradeID: 312370290,
		},
		poloniex.PrivateTradeHistoryEntry{
			Date:          "2018-01-02 05:58:12",
			Rate:          0.000470,
			Amount:        93.661619,
			Total:         0.044021,
			OrderNumber:   55587238287,
			Type:          "sell",
			GlobalTradeID: 312370156,
		},
	},
}

type (
	TPoloniexMock struct {
		ErrState bool
		emitter *emission.Emitter
	}
)
//public
func NewPoloniexMock() *TPoloniexMock{
	pm := new(TPoloniexMock)
	pm.emitter = emission.NewEmitter()
	pm.ErrState = false
	return pm
}
func (pm *TPoloniexMock) Ticker() (ticker poloniex.Ticker, err error) {
	if pm.ErrState {err = errors.New("Mock Poloniex Ticker Error")}
	ticker = tickers
	return ticker,err
}
func (pm *TPoloniexMock) Subscribe(chid string) (err error) {return err}
func (pm *TPoloniexMock) StartWS(){/*заглушка*/}
func (pm *TPoloniexMock) On(event interface{}, listener interface{}) *emission.Emitter{
	return pm.emitter.On(event, listener)
}
func (pm *TPoloniexMock) Buy(pair string, rate, amount float64) (buy poloniex.Buy, err error){
	if pm.ErrState {err = errors.New("Mock Poloniex Buy Error")}
	time.Sleep(time.Second)
	buy = poloniex.Buy{OrderNumber:getOrderID()}
	openOrdersAll[pair] = append(openOrdersAll[pair],poloniex.OpenOrder{OrderNumber: buy.OrderNumber,Type:"buy",Rate:rate,Amount:amount,Total:rate * amount,})
	pp.Println("---Order Buy размещён "+strconv.Itoa(int(buy.OrderNumber))+" amount "+strconv.FormatFloat(amount,'g',-1,64))
	return buy,err
}
func (pm *TPoloniexMock) Sell(pair string, rate, amount float64) (sell poloniex.Sell, err error){
	if pm.ErrState {err = errors.New("Mock Poloniex Sell Error")}
	time.Sleep(time.Second)
	sell = poloniex.Sell{Buy: poloniex.Buy{OrderNumber:getOrderID()}}
	openOrdersAll[pair] = append(openOrdersAll[pair],poloniex.OpenOrder{OrderNumber: sell.OrderNumber,Type:"sell",Rate:rate,Amount:amount,Total:rate * amount,})
	pp.Println("---Order Sell размещён "+strconv.Itoa(int(sell.OrderNumber))+" amount "+strconv.FormatFloat(amount,'g',-1,64))
	return sell,err
}
func (pm *TPoloniexMock) SellPostOnly(pair string, rate, amount float64) (sell poloniex.Sell, err error){
	if pm.ErrState {err = errors.New("Mock Poloniex SellPostOnly Error")}
	time.Sleep(time.Second)
	sell = poloniex.Sell{Buy: poloniex.Buy{OrderNumber:getOrderID()}}
	openOrdersAll[pair] = append(openOrdersAll[pair],poloniex.OpenOrder{OrderNumber: sell.OrderNumber,Type:"sell",Rate:rate,Amount:amount,Total:rate * amount,})
	pp.Println("---Order Sell Post Only размещён "+strconv.Itoa(int(sell.OrderNumber)))
	return sell,err
}
func (pm *TPoloniexMock) CancelOrder(orderNumber int64) (success bool, err error){
	time.Sleep(time.Second)
	success = true
	//changeInOpenOrders(orderNumber,0)
	if pm.ErrState {err = errors.New("Mock Poloniex CancelOrder Error");success = false}
	pp.Println("---Order отменён "+strconv.Itoa(int(orderNumber)))
	return success,err
}
func (pm *TPoloniexMock) PrivateTradeHistoryAll(dates ...int64) (history poloniex.PrivateTradeHistoryAll, err error) {
	return tradeHistory, nil
}
//private
func getOrderID() int64{return rand.Int63n(99999999999)+100000000000}
func (pm *TPoloniexMock) changeInTicker(pair string, ask,bid float64){
	t := poloniex.WSTicker{Pair : pair,Ask  : ask,Bid  : bid,}
	pm.emitter.Emit("ticker", t)
	pp.Println("---Изменения в тикере "+pair+" bid:"+strconv.FormatFloat(bid,'g',-1,64)+" ask:"+strconv.FormatFloat(ask,'g',-1,64))
}
func changeInOpenOrders(orderNumber int64,amount float64){
	for aoKey,aoVal := range openOrdersAll{
		for oKey,oVal := range aoVal{
			if oVal.OrderNumber == orderNumber{
				if amount == 0{
					copy(aoVal[oKey:], aoVal[oKey+1:])
					aoVal = aoVal[:len(aoVal)-1]
					openOrdersAll[aoKey] = aoVal
					pp.Println("---order "+strconv.Itoa(int(orderNumber))+" исполнен")
				}else{
					openOrdersAll[aoKey][oKey].Amount = amount
					pp.Println("---ордер доисполнился "+strconv.FormatFloat(amount,'g',-1,64))
				}
				return
			}
		}
	}
}

func addToTradeHist(pair string,orderNumber int64,amount float64){
	th := poloniex.PrivateTradeHistoryEntry{
		Date:          time.Now().Format("2006-01-02 15:04:05"),
		Rate:          0,
		Amount:        amount,
		Total:         0,
		OrderNumber:   orderNumber,
		Type:          "",
		GlobalTradeID: 0,
	}
	if len(tradeHistory[pair])==0{
		tradeHistory[pair] = []poloniex.PrivateTradeHistoryEntry{th}
	}else{
		tradeHistory[pair] = append(tradeHistory[pair],th)
	}
	pp.Println("---ордер "+strconv.Itoa(int(orderNumber))+" исполнился на "+strconv.FormatFloat(amount,'g',-1,64))
}

//Tests
func (pm *TPoloniexMock)Test1(d *TDeal){
	//1 ордер исполнился полностью
	//ценник в зоне а  -  вывод
	//ценник в зоне б  -  вывод
	//ценник сорвал тэйк - финал
	//задержечка
	time.Sleep(15*time.Second)
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.004)
	time.Sleep(3*time.Second)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.002)
	time.Sleep(3*time.Second)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.002)
	time.Sleep(3*time.Second)
	//ценник в зоне а  -  вывод
	pm.changeInTicker("USDT_BTC",6100,6000)
	time.Sleep(3*time.Second)
	//ценник в зоне б  -  вывод
	pm.changeInTicker("USDT_BTC",4600,4500)
	time.Sleep(3*time.Second)
	//ордер исполнен
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.002)
	time.Sleep(3*time.Second)
	pm.changeInTicker("USDT_BTC",6600,6500)
	time.Sleep(3*time.Second)
	//ценник сорвал тэйк - финал
	pm.changeInTicker("USDT_BTC",11100,11000)
	f := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.01)
				break
			}
		}
	}
	go f()
	time.Sleep(3*time.Second)

	//"a state"
	//"Пробую поставить order: USDT_BTC buy amount: 0.01 rate: 5000"
	//"---Order Buy размещён 163497840744 amount 0.01"
	//"a state"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 163497840744 исполнился на 0.004"
	//"a state"
	//"---ордер 163497840744 исполнился на 0.002"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.004 rate: 11000"
	//"---Order Sell размещён 126152206411 amount 0.004"
	//"---ордер 163497840744 исполнился на 0.002"
	//"a state"
	//"Пробую снять tp: 126152206411"
	//"---Order отменён 126152206411"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.008 rate: 11000"
	//"---Изменения в тикере USDT_BTC bid:6000 ask:6100"
	//"---Order Sell размещён 116284565720 amount 0.008"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:4500 ask:4600"
	//"b state"
	//"Пробую снять tp: 116284565720"
	//"---ордер 163497840744 исполнился на 0.002"
	//"---Order отменён 116284565720"
	//"---Изменения в тикере USDT_BTC bid:6500 ask:6600"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.01 rate: 11000"
	//"---Изменения в тикере USDT_BTC bid:11000 ask:11100"
	//"---Order Sell размещён 140869931816 amount 0.01"
	//"---ордер 140869931816 исполнился на 0.01"
	//"Закрываю сделку"

}

func (pm *TPoloniexMock)Test2(d *TDeal){
	//2 ордер исполнился полностью
	//	ценник в зоне б
	//	ценник сорвал лосс - финал

	//2 ордер исполнился полностью
	time.Sleep(15*time.Second)
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.01)
	time.Sleep(3*time.Second)

	//ценник в зоне б
	pm.changeInTicker("USDT_BTC",4600,4500)
	time.Sleep(6*time.Second)

	//ценник в зоне c
	pm.changeInTicker("USDT_BTC",4100,4000)
	time.Sleep(3*time.Second)

	//ценник в зоне c
	pm.changeInTicker("USDT_BTC",4000,3900)
	time.Sleep(3*time.Second)

	//ценник в зоне c
	pm.changeInTicker("USDT_BTC",3900,3800)
	pp.Println("6 sec")
	time.Sleep(6*time.Second)

	f := func() {
		for {
			if d.StopLoss != nil && d.StopLoss.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.StopLoss.OrderNumber,0.01)
				break
			}
		}
	}
	go f()
	//"a state"
	//"Пробую поставить order: USDT_BTC buy amount: 0.01 rate: 5000"
	//"---Order Buy размещён 163075536533 amount 0.01"
	//"a state"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 163075536533 исполнился на 0.01"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:4500 ask:4600"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.01 rate: 11000"
	//"---Order Sell размещён 188315147351 amount 0.01"
	//"b state"
	//"Пробую снять tp: 188315147351"
	//"---Order отменён 188315147351"
	//"---Изменения в тикере USDT_BTC bid:4000 ask:4100"
	//"c state"
	//"---Изменения в тикере USDT_BTC bid:3900 ask:4000"
	//"---Order Sell Post Only размещён 160271644992"
	//"Пробую поставить slp PostOnly: USDT_BTC buy amount: 0.01 rate: 4000"
	//"c state"
	//"Пробую снять slp: 160271644992"
	//"---Изменения в тикере USDT_BTC bid:3800 ask:3900"
	//"6 sec"
	//"---Order отменён 160271644992"
	//"---Order Sell Post Only размещён 102279489837"
	//"Пробую поставить slp PostOnly: USDT_BTC buy amount: 0.01 rate: 3800"
	//"c state"
	//"---ордер 102279489837 исполнился на 0.01"
	//"Закрываю сделку"
}

func (pm *TPoloniexMock)TT(d *TDeal){
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.01)
	time.Sleep(5*time.Second)
	pm.changeInTicker("USDT_BTC",6100,6000)
	time.Sleep(5*time.Second)
	f1 := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.006)
				break
			}
		}
	}
	go f1()
}

func (pm *TPoloniexMock)Test3(d *TDeal,m *TMoneyManager){
	//3 ордер исполнился частично
	//	ценник в зоне а  -  вывод
	//	ордер доисполнился частично - вывод
	//	частичный тэйк - вывод
	//	ордер доисполнился полностью - вывод
	//	тэйк - финал
	//3 ордер исполнился частично
	time.Sleep(15*time.Second)
//*
		pm.changeInTicker("USDT_BTC",5100,5000)
		addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.01)
		time.Sleep(5*time.Second)
//*/

	//	ценник в зоне а  -  вывод
	pm.changeInTicker("USDT_BTC",6100,6000)
	time.Sleep(5*time.Second)



//*
	//	ордер доисполнился частично - вывод
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.003)
	time.Sleep(5*time.Second)


	//	частичный тэйк - вывод
	pm.changeInTicker("USDT_BTC",11000,10900)
	f := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.004)
				break
			}
		}
	}
	go f()
	time.Sleep(6*time.Second)



	//	ордер доисполнился полностью - вывод
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.002)
	time.Sleep(5*time.Second)

//os.Exit(0)

//*/

//*
	//	тэйк - финал
	f1 := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.006)
				break
			}
		}
	}
	go f1()
//*/
	//"a state"
	//"Пробую поставить order: USDT_BTC buy amount: 0.01 rate: 5000"
	//"---Order Buy размещён 143253647301 amount 0.01"
	//"a state"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 143253647301 исполнился на 0.005"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.005 rate: 11000"
	//"---Изменения в тикере USDT_BTC bid:6000 ask:6100"
	//"---Order Sell размещён 186181439313 amount 0.005"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 143253647301 исполнился на 0.003"
	//"a state"
	//"Пробую снять tp: 186181439313"
	//"---Order отменён 186181439313"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.008 rate: 11000"
	//"---Order Sell размещён 122420468361 amount 0.008"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:10900 ask:11000"
	//"---ордер 122420468361 исполнился на 0.004"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 143253647301 исполнился на 0.002"
	//"a state"
	//"Пробую снять tp: 122420468361"
	//"---Order отменён 122420468361"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.006 rate: 11000"
	//"---Order Sell размещён 172859257802 amount 0.006"
	//"---ордер 172859257802 исполнился на 0.006"
	//"Закрываю сделку"
}

func (pm *TPoloniexMock)Test4(d *TDeal){
	//4 ордер исполнился частично
	//	ценник в зоне а  -  вывод
	//	тэйк исполнился полностью
	//	ордер исполнился полностью
	//	тэйк - финал

	//3 ордер исполнился частично
	time.Sleep(15*time.Second)
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.005)
	time.Sleep(5*time.Second)

	//	ценник в зоне а  -  вывод
	pm.changeInTicker("USDT_BTC",6100,6000)
	time.Sleep(5*time.Second)

	//	частичный тэйк - вывод
	pm.changeInTicker("USDT_BTC",11000,10900)
	f := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.005)
				break
			}
		}
	}
	go f()
	time.Sleep(6*time.Second)

	//	ордер доисполнился полностью - вывод
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.005)
	time.Sleep(5*time.Second)

	//	тэйк - финал
	pm.changeInTicker("USDT_BTC",11000,10900)
	f1 := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.005)
				break
			}
		}
	}
	go f1()
	time.Sleep(6*time.Second)

	//"a state"
	//"Пробую поставить order: USDT_BTC buy amount: 0.01 rate: 5000"
	//"---Order Buy размещён 182227140417 amount 0.01"
	//"a state"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 182227140417 исполнился на 0.005"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.005 rate: 11000"
	//"---Изменения в тикере USDT_BTC bid:6000 ask:6100"
	//"---Order Sell размещён 136362158453 amount 0.005"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:10900 ask:11000"
	//"---ордер 136362158453 исполнился на 0.005"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 182227140417 исполнился на 0.005"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.005 rate: 11000"
	//"---Изменения в тикере USDT_BTC bid:10900 ask:11000"
	//"---Order Sell размещён 174845407387 amount 0.005"
	//"---ордер 174845407387 исполнился на 0.005"
	//"Закрываю сделку"
}

func (pm *TPoloniexMock)Test5(d *TDeal){
	//5 ордер исполнился частично
	//	ценник в зоне а  -  вывод
	//	тэйк исполнился полностью
	//	ордер исполнился полностью
	//	loss - финал

	time.Sleep(15*time.Second)
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.005)
	time.Sleep(5*time.Second)

	//	ценник в зоне а  -  вывод
	pm.changeInTicker("USDT_BTC",6100,6000)
	time.Sleep(5*time.Second)

	//	частичный тэйк - вывод
	pm.changeInTicker("USDT_BTC",11000,10900)
	f := func() {
		for {
			if d.TakeProfit != nil && d.TakeProfit.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.TakeProfit.OrderNumber,0.005)
				break
			}
		}
	}
	go f()
	time.Sleep(6*time.Second)

	//	ордер доисполнился полностью - вывод
	pm.changeInTicker("USDT_BTC",5100,5000)
	addToTradeHist("USDT_BTC",d.Order.OrderNumber,0.005)
	time.Sleep(5*time.Second)

	//	тэйк - финал
	pm.changeInTicker("USDT_BTC",4100,4000)
	time.Sleep(6*time.Second)
	pm.changeInTicker("USDT_BTC",3100,3000)
	time.Sleep(6*time.Second)
	f1 := func() {
		for {
			if d.StopLoss != nil && d.StopLoss.OrderNumber != 0{
				addToTradeHist("USDT_BTC",d.StopLoss.OrderNumber,0.005)
				break
			}
		}
	}
	go f1()
	time.Sleep(6*time.Second)
	//"Пробую поставить order: USDT_BTC buy amount: 0.01 rate: 5000"
	//"---Order Buy размещён 119154860564 amount 0.01"
	//"a state"
	//"a state"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 119154860564 исполнился на 0.005"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.005 rate: 11000"
	//"---Order Sell размещён 194468114062 amount 0.005"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:6000 ask:6100"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:10900 ask:11000"
	//"---ордер 194468114062 исполнился на 0.005"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:5000 ask:5100"
	//"---ордер 119154860564 исполнился на 0.005"
	//"a state"
	//"Пробую поставить tp: USDT_BTC sell amount: 0.005 rate: 11000"
	//"---Order Sell размещён 159228249137 amount 0.005"
	//"a state"
	//"---Изменения в тикере USDT_BTC bid:4000 ask:4100"
	//"c state"
	//"Пробую снять tp: 159228249137"
	//"---Order отменён 159228249137"
	//"---Изменения в тикере USDT_BTC bid:3000 ask:3100"
	//"---Order Sell Post Only размещён 123147213462"
	//"Пробую поставить slp PostOnly: USDT_BTC buy amount: 0.005 rate: 4000"
	//"c state"
	//"Пробую снять slp: 123147213462"
	//"---Order отменён 123147213462"
	//"---Order Sell Post Only размещён 115814933707"
	//"Пробую поставить slp PostOnly: USDT_BTC buy amount: 0.005 rate: 3000"
	//"c state"
	//"---ордер 115814933707 исполнился на 0.005"
	//"Закрываю сделку"
}

