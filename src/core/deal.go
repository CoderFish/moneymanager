package core

import (
	"github.com/k0kubun/pp"
	"time"
	"github.com/google/uuid"
)

type(
	TExecutedOrders struct {
		ExecOrderAmount float64
		ExecTakeAmount float64
		ExecLossAmount float64
		LastTakeOrderNumber int64
		LastLostOrderNumber int64
	}
	TDeal struct {
		mm *TMoneyManager
		Id string
		StartDT time.Time
		Pair string
		OrderType string
		Amount float64
		OrderPrice float64
		TakeProfitPrice float64
		StopLossPrice float64
		Order *TOrder
		TakeProfit *TOrder
		StopLoss *TOrder
		ExecutedOrders TExecutedOrders
		cancelDealCallBack func(reason string,d *TDeal)
		closeDealCallBack func(d *TDeal)
	}
)

func NewDeal(mm *TMoneyManager,pair,orderType string,price,amount,tp,sl float64) *TDeal {
	d := new(TDeal)
	d.Id = uuid.New().String()
	d.StartDT = time.Now()
	d.mm = mm
	d.Pair = pair
	d.OrderType = orderType
	d.OrderPrice = price
	d.Amount = amount
	d.TakeProfitPrice = tp
	d.StopLossPrice = sl
	return d
}

func (d *TDeal) AddCallBacks(cancelCB func (reason string,d *TDeal),closeCB func(d *TDeal)){
	d.cancelDealCallBack = cancelCB
	d.closeDealCallBack = closeCB
}

func (d *TDeal)PutOrder(orderKind string, rate, amount float64) {
	switch orderKind {
		case "order"    : {d.Order = NewOrder(d.OrderType,orderKind,d.Pair,rate,amount,d.mm)}
		case "tp"       : {d.TakeProfit = NewOrder("sell",orderKind,d.Pair,rate,amount,d.mm)}
		case "sl","slp" : {d.StopLoss = NewOrder("buy",orderKind,d.Pair,rate,amount,d.mm)}
	}
	j := NewJob("normal",1,func(){
		var err error
		switch orderKind {
			case "order": err = d.Order.PutOrder()
			case "tp"   : err = d.TakeProfit.PutOrder()
			case "sl"   : err = d.StopLoss.PutOrder()
			case "slp"  : err = d.StopLoss.SellPostOnly()
		}
		//а вот тут нужно ещё решить что делать если стоп или тэйк не ставиться
		if err != nil {d.cancelDealCallBack(err.Error(),d);return}
	})
	d.mm.queue.AddJobs(j)
	d.mm.Serialize()
}

func (d *TDeal) getABCState() (state string){
	//      'a' когда заявки на продажу выше входа
	//      'b' когда заявка на продажу ниже входа
	//      'c' когда заявки на покупку ниже или равны стопу
	if d.OrderPrice<d.mm.ticker[d.Pair].Ask {state = "a"}
	if d.OrderPrice>=d.mm.ticker[d.Pair].Ask {state = "b"}
	if d.StopLossPrice>=d.mm.ticker[d.Pair].Bid {state = "c"}
	return state
}

func (d *TDeal) getOrderExecutedAmmount(o *TOrder) (amount float64){
	if o != nil && len((d.mm.tradeHist)[d.Pair])>0{
		for _,val := range (d.mm.tradeHist)[d.Pair] {
			if val.OrderNumber == o.OrderNumber{
				amount = amount + val.Amount
			}
		}
	}
	return amount
}

func (d *TDeal) getTPSLAmount() float64{
	return d.ExecutedOrders.ExecOrderAmount - d.ExecutedOrders.ExecTakeAmount - d.ExecutedOrders.ExecLossAmount
}

func (d *TDeal) ReCalcOrders(){
	if d != nil {
		//если есть тикер
		if len(d.mm.ticker) != 0 {
			//сумма в TradeHist для order не равна ExecOrderAmount
			//записываем в ExecOrderAmount
			//сумма в TradeHist для tp не равна ExecTPAmount
			//записываем в ExecTPAmount
			//сумма в TradeHist для sl не равна ExecSLAmount
			//записываем в ExecSLAmount
			oa := d.getOrderExecutedAmmount(d.Order)
			existOrderChanges, existSLChanges := false, false
			if oa != d.ExecutedOrders.ExecOrderAmount {
				d.ExecutedOrders.ExecOrderAmount = oa
				existOrderChanges = true
			}
			oa = d.getOrderExecutedAmmount(d.TakeProfit)
			if oa != d.ExecutedOrders.ExecTakeAmount || (d.TakeProfit != nil && d.ExecutedOrders.LastTakeOrderNumber != d.TakeProfit.OrderNumber) {
				d.ExecutedOrders.ExecTakeAmount = d.ExecutedOrders.ExecTakeAmount + oa
				d.ExecutedOrders.LastTakeOrderNumber = d.TakeProfit.OrderNumber
				if oa == d.TakeProfit.Amount {
					d.TakeProfit.OrderNumber = 0
				}
			}
			oa = d.getOrderExecutedAmmount(d.StopLoss)
			if oa != d.ExecutedOrders.ExecLossAmount || (d.StopLoss != nil && d.ExecutedOrders.LastLostOrderNumber != d.StopLoss.OrderNumber) {
				d.ExecutedOrders.ExecLossAmount = d.ExecutedOrders.ExecLossAmount + oa
				d.ExecutedOrders.LastLostOrderNumber = d.StopLoss.OrderNumber
				if oa == d.StopLoss.Amount {
					d.StopLoss.OrderNumber = 0
				}
				existSLChanges = true
			}
			//если выполнен ордер и сумма выполненных tp sl ExecAmount = выполненному ордеру
			//закрываем сделку, выходим
			if d.ExecutedOrders.ExecOrderAmount == d.Order.Amount && (d.ExecutedOrders.ExecLossAmount+d.ExecutedOrders.ExecTakeAmount) == d.Order.Amount {
				d.closeDealCallBack(d)
				return
			}
			//посчитали tp/sl (*1 есть/нет изменений)
			tpslAmount := d.getTPSLAmount()
			abc := d.getABCState()
			if abc == "a" { //выше цены покупки
				//если есть sl отменяем
				if d.StopLoss != nil {
					d.StopLoss.CanceOrder()
				}
				//тэйк стоит отменяем (только если есть изменения в *1)
				if d.TakeProfit != nil && (existOrderChanges || existSLChanges) {
					d.TakeProfit.CanceOrder()
				}
				//тэйка нет размещаем
				if tpslAmount != 0 && (d.TakeProfit == nil || d.TakeProfit.OrderNumber == 0) {
					d.PutOrder("tp", d.TakeProfitPrice, tpslAmount)
				}
			} else
			if abc == "b" { //ниже цены покупки
				//если есть sl отменяем
				if d.StopLoss != nil {
					d.StopLoss.CanceOrder()
				}
				//есть тэйк отменяем
				if d.TakeProfit != nil {
					d.TakeProfit.CanceOrder()
				}
			} else
			if abc == "c" && d.StopLossPrice != 0 { //пиздец
				//есть tp отменяем
				if d.TakeProfit != nil {
					d.TakeProfit.CanceOrder()
				}
				//если был стоп и bid ушёл ниже отменяем стоп
				if d.StopLoss != nil && d.StopLoss.OrderNumber != 0 && d.mm.ticker[d.Pair].Bid < d.StopLoss.Rate {
					d.StopLoss.CanceOrder()
				}
				//если нет sl ставим по бид PostOnly Sell Order
				if tpslAmount != 0 && (d.StopLoss == nil || d.StopLoss.OrderNumber == 0) {
					d.PutOrder("slp", d.mm.ticker[d.Pair].Bid, tpslAmount)
				}
			}
			existOrderChanges, existSLChanges = false, false
			d.mm.Serialize()
		} else {
			pp.Println("Нет тикера")
		}
	}
}
