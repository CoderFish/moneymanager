package core

import (
	"io/ioutil"
	"encoding/json"
	"github.com/k0kubun/pp"
)

type (
	TConfig struct {
		Key string
		Secret string
		Redis_Host string
		MM_ID string
	}
)

func NewConfig(fn string)(c *TConfig){
	js, err := ioutil.ReadFile(fn)
	if err != nil {pp.Println(err)}
	err = json.Unmarshal(js, &c)
	if err != nil {pp.Println(err)}
	return c
}


