package core

import (
	"github.com/pharrisee/poloniex-api"
	"github.com/chuckpreslar/emission"
)

type (
	IExchange interface {
		Ticker() (ticker poloniex.Ticker, err error)
		Subscribe(chid string) error
		StartWS()
		On(event interface{}, listener interface{}) *emission.Emitter
		Buy(pair string, rate, amount float64) (buy poloniex.Buy, err error)
		Sell(pair string, rate, amount float64) (sell poloniex.Sell, err error)
		SellPostOnly(pair string, rate, amount float64) (sell poloniex.Sell, err error)
		CancelOrder(orderNumber int64) (success bool, err error)
		PrivateTradeHistoryAll(dates ...int64) (history poloniex.PrivateTradeHistoryAll, err error)
	}
	IDB interface {

	}
)