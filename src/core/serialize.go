package core

import (
	"encoding/json"
)

/************************************************************************
	принцип сериализации
	main.go запускается с параметром UID
	после запуска лезем в DB (redis/или ещё что)
	если там есть структура с таким UID десериализуем
	если нет создаём новую, сериализуем
	при любых изменениях сериализуем

	мэнеджер процессов (supervisor) следит за падением и перезапуском
/************************************************************************/

func (mm *TMoneyManager)Serialize(){
//*
	if !mm.fCreating {
		r,_ := json.Marshal(mm)
		//err :=
		mm.redis.Set("MMID:"+mm.ID,string(r),0)
		//if err != nil {pp.Println(err)}
	}
//*/
}

func (mm *TMoneyManager)DeSerialize(){
	j,_ := mm.redis.Get("MMID:"+mm.ID).Result()
	//if err != nil {pp.Println(err)}
	if j != "" {json.Unmarshal([]byte(j),mm)}
	for key,val := range mm.Deals {
		mm.Deals[key].AddCallBacks(mm.cancelCB,mm.closeCB)
		val.mm = mm
		if val.Order != nil{val.Order.mm = mm}
		if val.TakeProfit != nil{val.TakeProfit.mm = mm}
		if val.StopLoss != nil{val.StopLoss.mm = mm}
	}
}