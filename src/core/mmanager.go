package core

import (
	"github.com/pharrisee/poloniex-api"
	"github.com/k0kubun/pp"
	"strconv"
	"time"
	"github.com/go-redis/redis"
)

type (
	TMoneyManager struct {
		ID        string
		exchange  IExchange
		db		  IDB
		queue     *TQueue
		redis     *redis.Client
		Deals     []*TDeal
		MaxDeals  int
		fCreating bool
		ticker    poloniex.Ticker
		tradeHist poloniex.PrivateTradeHistoryAll
		closeCB func(d *TDeal)
		cancelCB func(reason string,d *TDeal)
	}
)

func NewMoneyManager(id string,e IExchange,q *TQueue,r *redis.Client,d IDB,maxDeals int) *TMoneyManager{
	mm := new(TMoneyManager)
	mm.fCreating = true
	mm.ID = id
	mm.exchange = e
	mm.queue = q
	mm.redis = r
	mm.db = d
	mm.MaxDeals = maxDeals
	mm.closeCB = func (d *TDeal){
		//Сообщение в бд
		pp.Println("Закрываю сделку ")
		mm.deleteDeal(d)
	}
	mm.cancelCB = func (reason string, d *TDeal){
		//пишем в бд
		mm.deleteDeal(d)
		pp.Println(reason)
	}
	mm.DeSerialize()
	mm.fCreating = false
	mm.tradeHist = make(poloniex.PrivateTradeHistoryAll)
	mm.getTicker()
	go mm.checkOrderTrades()
	return mm
}

func (mm *TMoneyManager) AddDeal(pair string, orderType string, price, amount, tp, sl float64) {
	if len(mm.Deals)<mm.MaxDeals {
		d := NewDeal(mm,pair,orderType,price,amount,tp,sl)
		d.AddCallBacks(mm.cancelCB,mm.closeCB)
		mm.Deals = append(mm.Deals,d)
		d.PutOrder("order",price,amount)
	}else{pp.Println("Достигнут предел открытых сделок "+strconv.Itoa(mm.MaxDeals))}
}

func (mm *TMoneyManager) getTicker(){
	ticker, err := mm.exchange.Ticker()
	if err != nil {pp.Println(err)}
	mm.ticker = ticker
	mm.exchange.Subscribe("ticker")
	mm.exchange.StartWS()
	mm.exchange.On("ticker", func(m poloniex.WSTicker) {
		tmp := mm.ticker[m.Pair]
		tmp.Last = m.Last
		tmp.Ask = m.Ask
		tmp.Bid = m.Bid
		tmp.BaseVolume = m.BaseVolume
		tmp.QuoteVolume = m.QuoteVolume
		mm.ticker[m.Pair] = tmp
	})
	//сообщение в бд подписался на тикер
}

func (mm *TMoneyManager) deleteDeal(d *TDeal){
	for i, v := range mm.Deals{
		if v == d{
			copy(mm.Deals[i:], mm.Deals[i+1:])
			mm.Deals[len(mm.Deals)-1] = nil
			mm.Deals = mm.Deals[:len(mm.Deals)-1]
			break
		}
	}
	mm.Serialize()
}

func (mm *TMoneyManager) getMinDT () (dt time.Time){
	for _,val := range mm.Deals{
		if dt.IsZero() || dt.After(val.StartDT){
			dt = val.StartDT
		}
	}
	return dt
}

func (mm *TMoneyManager) getOrderTrades(){
	var err error
	mm.tradeHist,err = mm.exchange.PrivateTradeHistoryAll(mm.getMinDT().Add(-24*60*60*time.Second).Unix(),time.Now().Add(24*60*60*time.Second).Unix())
	if err !=nil {pp.Println(err)}
	defer mm.queue.AddJobs(NewJob("high",3, mm.getOrderTrades))
	//сообщаем сделкам что нужно пересчитать ордера если что-то изменилоь
	for _,val := range mm.Deals {val.ReCalcOrders()}
}

func (mm *TMoneyManager) checkOrderTrades(){
	mm.queue.AddJobs(NewJob("high",0,mm.getOrderTrades))
}